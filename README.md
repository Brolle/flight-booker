Have you ever tried doing a system for online booking? For a flight maybe? If yes, then yu will surely have the idea on how it works. But if you have not, then this one will be your guide in understanding the process. It may sound complicated for you, but believe me, if you get the chance to study more about it, this won't sound the way it is. 

If you want to know more about online processes, you can find more [here](http://99onlinepoker.net).

##Project: Flight Booker


My implementation of the Flight Booker project from the Odin Curriculum.

Project focus: building advanced forms with search and nested attributes.

##Objective
>In this project, you'll get a chance to tackle some more advanced forms. This is the kind of thing you'll have to work with when handling user orders for anything more complicated than an e-book.

Build a flight booker, where user selects flight based on date, departure and arrival airports, and # of passengers.  User enters passenger detail on booking page and receives confirmation.